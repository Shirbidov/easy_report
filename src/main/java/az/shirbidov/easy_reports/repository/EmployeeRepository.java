package az.shirbidov.easy_reports.repository;

import az.shirbidov.easy_reports.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
