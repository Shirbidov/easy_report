package az.shirbidov.easy_reports.repository;

import az.shirbidov.easy_reports.domain.SubDepartment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubDepartmentRepository extends JpaRepository<SubDepartment,Long> {
}
