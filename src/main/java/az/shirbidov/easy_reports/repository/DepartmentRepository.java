package az.shirbidov.easy_reports.repository;

import az.shirbidov.easy_reports.domain.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department, Long> {
}
