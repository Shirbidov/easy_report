package az.shirbidov.easy_reports;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasyReportsApplication {

	public static void main(String[] args) {
		SpringApplication.run(EasyReportsApplication.class, args);
	}

}
