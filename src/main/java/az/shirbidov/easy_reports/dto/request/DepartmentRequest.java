package az.shirbidov.easy_reports.dto.request;

import lombok.Data;

@Data
public class DepartmentRequest {
    private Long id;
    private String name;
}
