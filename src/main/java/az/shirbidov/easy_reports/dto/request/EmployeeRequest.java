package az.shirbidov.easy_reports.dto.request;

import az.shirbidov.easy_reports.domain.Department;
import lombok.Data;

@Data

public class EmployeeRequest {
    private Long id;
    private String name;
    private String surname;
    private Department department;


}
