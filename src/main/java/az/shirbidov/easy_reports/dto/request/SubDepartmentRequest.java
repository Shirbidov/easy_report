package az.shirbidov.easy_reports.dto.request;

import az.shirbidov.easy_reports.domain.Department;

import lombok.Data;

@Data
public class SubDepartmentRequest {

    private Long id;
    private String name;
    private Department department;
}
