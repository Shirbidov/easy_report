package az.shirbidov.easy_reports.dto.response;

import az.shirbidov.easy_reports.domain.Department;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EmployeeResponse {

    private String name;
    private String surname;
    private Department department;
}
