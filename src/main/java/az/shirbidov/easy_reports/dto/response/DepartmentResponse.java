package az.shirbidov.easy_reports.dto.response;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class DepartmentResponse {

    private String name;
}
