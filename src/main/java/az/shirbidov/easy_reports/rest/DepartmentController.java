package az.shirbidov.easy_reports.rest;

import az.shirbidov.easy_reports.dto.request.DepartmentRequest;
import az.shirbidov.easy_reports.dto.response.DepartmentResponse;
import az.shirbidov.easy_reports.service.service.DepartmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/department")
public class DepartmentController {

    private final DepartmentService departmentService;

    @GetMapping("/{id}")
    public DepartmentResponse getDepartmentById(@PathVariable Long id){
        return departmentService.getDepartmentById(id);
    }

    @PostMapping
    public DepartmentResponse createDepartment(@RequestBody @Validated DepartmentRequest departmentRequest){
        return departmentService.addDepartment(departmentRequest);
    }

    @DeleteMapping("/{id}")
    public void deleteEmployeeById(@PathVariable Long id){
        departmentService.deleteDepartment(id);
    }

    @PutMapping("/{id}")
    public DepartmentResponse updateDepartment(@PathVariable Long id, @RequestBody DepartmentRequest departmentRequest){
        return departmentService.updateDepartment(id, departmentRequest);
    }

}
