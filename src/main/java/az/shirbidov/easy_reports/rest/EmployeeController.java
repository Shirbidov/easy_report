package az.shirbidov.easy_reports.rest;

import az.shirbidov.easy_reports.dto.request.EmployeeRequest;
import az.shirbidov.easy_reports.dto.response.EmployeeResponse;
import az.shirbidov.easy_reports.service.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/employee")
public class EmployeeController {

    private final EmployeeService employeeService;

    @GetMapping("/{id}")
    public EmployeeResponse getEmployeeById(@PathVariable Long id){
        return employeeService.getEmployeeById(id);
    }

    @PostMapping
    public void createEmployee(@RequestBody @Validated EmployeeRequest employeeRequest){
         employeeService.createEmployee(employeeRequest);
    }

    @DeleteMapping("/{id}")
    public void deleteEmployeeById(@PathVariable Long id){
    employeeService.deleteEmployee(id);
    }

    @PutMapping("/{id}")
    public EmployeeResponse updateEmployeeById(@PathVariable Long id ,@RequestBody EmployeeRequest employeeRequest){
        return employeeService.updateEmployee(id,employeeRequest);
    }


}
