package az.shirbidov.easy_reports.service.service;

import az.shirbidov.easy_reports.dto.request.EmployeeRequest;
import az.shirbidov.easy_reports.dto.response.EmployeeResponse;

public interface EmployeeService {

    void createEmployee(EmployeeRequest employeeRequest);

    EmployeeResponse updateEmployee(Long id, EmployeeRequest employeeRequest);

    void deleteEmployee(Long id);

    EmployeeResponse getEmployeeById(Long id);

}
