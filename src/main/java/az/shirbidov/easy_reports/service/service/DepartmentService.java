package az.shirbidov.easy_reports.service.service;

import az.shirbidov.easy_reports.dto.request.DepartmentRequest;
import az.shirbidov.easy_reports.dto.response.DepartmentResponse;
import az.shirbidov.easy_reports.repository.DepartmentRepository;

public interface DepartmentService {

    DepartmentResponse addDepartment(DepartmentRequest departmentRequest);
    DepartmentResponse updateDepartment(Long id ,DepartmentRequest departmentRequest);
    void deleteDepartment(Long id);
    DepartmentResponse getDepartmentById(Long id);

}
