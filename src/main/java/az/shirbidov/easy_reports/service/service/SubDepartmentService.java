package az.shirbidov.easy_reports.service.service;

import az.shirbidov.easy_reports.domain.SubDepartment;
import az.shirbidov.easy_reports.dto.request.SubDepartmentRequest;
import az.shirbidov.easy_reports.dto.response.SubDepartmentResponse;

public interface SubDepartmentService {
    SubDepartmentResponse createSubDepartment(SubDepartmentRequest subDepartmentRequest);

    SubDepartmentResponse updateSubDepartment(SubDepartmentRequest subDepartmentRequest);

    void deleteSubDepartment(Long id);

    SubDepartmentResponse getSubDepartmentById(Long id);

}
