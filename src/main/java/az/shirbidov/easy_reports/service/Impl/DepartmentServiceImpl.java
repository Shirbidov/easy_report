package az.shirbidov.easy_reports.service.Impl;

import az.shirbidov.easy_reports.domain.Department;
import az.shirbidov.easy_reports.dto.request.DepartmentRequest;
import az.shirbidov.easy_reports.dto.response.DepartmentResponse;
import az.shirbidov.easy_reports.repository.DepartmentRepository;
import az.shirbidov.easy_reports.service.service.DepartmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DepartmentServiceImpl implements DepartmentService {

    private final DepartmentRepository departmentRepository;

    @Override
    public DepartmentResponse addDepartment(DepartmentRequest departmentRequest) {
        Department department =
                Department
                        .builder()
                        .name(departmentRequest.getName())
                        .build();

        final Department departmentSaved = departmentRepository.save(department);
        return DepartmentResponse
                .builder()
                .name(departmentSaved.getName())
                .build();
    }

    @Override
    public DepartmentResponse updateDepartment(Long id , DepartmentRequest departmentRequest) {
        Department department =
                Department
                        .builder()
                        .id(id)
                        .name(departmentRequest.getName())
                        .build();
        final Department departmentSaved = departmentRepository.save(department);
        return DepartmentResponse
                .builder()
                .name(departmentSaved.getName())
                .build();
    }

    @Override
    public void deleteDepartment(Long id) {
    departmentRepository.deleteById(id);
    }

    @Override
    public DepartmentResponse getDepartmentById(Long id) {

        final Department department = departmentRepository.findById(id)
                .orElseThrow();

        return DepartmentResponse
                .builder()
                .name(department.getName())
                .build();
    }
}
