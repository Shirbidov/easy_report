package az.shirbidov.easy_reports.service.Impl;

import az.shirbidov.easy_reports.domain.SubDepartment;
import az.shirbidov.easy_reports.dto.request.SubDepartmentRequest;
import az.shirbidov.easy_reports.dto.response.SubDepartmentResponse;
import az.shirbidov.easy_reports.repository.SubDepartmentRepository;
import az.shirbidov.easy_reports.service.service.SubDepartmentService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SubDepartmentImpl implements SubDepartmentService {

    private final ModelMapper modelMapper;
    private final SubDepartmentRepository subDepartmentRepository;

    @Override
    public SubDepartmentResponse createSubDepartment(SubDepartmentRequest subDepartmentRequest) {
        SubDepartment subDepartment = modelMapper.map(subDepartmentRequest, SubDepartment.class);
        subDepartmentRepository.save(subDepartment);
        return modelMapper.map(subDepartment, SubDepartmentResponse.class);

    }

    @Override
    public SubDepartmentResponse updateSubDepartment(SubDepartmentRequest subDepartmentRequest) {
        SubDepartment subDepartment = modelMapper.map(subDepartmentRequest, SubDepartment.class);
        subDepartmentRepository.save(subDepartment);
        return modelMapper.map(subDepartment, SubDepartmentResponse.class);

    }

    @Override
    public void deleteSubDepartment(Long id) {
        subDepartmentRepository.deleteById(id);
    }

    @Override
    public SubDepartmentResponse getSubDepartmentById(Long id) {
        return null;
    }
}
