package az.shirbidov.easy_reports.service.Impl;

import az.shirbidov.easy_reports.domain.Employee;
import az.shirbidov.easy_reports.dto.request.EmployeeRequest;
import az.shirbidov.easy_reports.dto.response.EmployeeResponse;
import az.shirbidov.easy_reports.repository.EmployeeRepository;
import az.shirbidov.easy_reports.service.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor

public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository ;
    private final ModelMapper modelMapper;

    /*
     public EmployeeResponse createEmployee(EmployeeRequest employeeRequest) {
        Employee employee =
                Employee
                        .builder()
                        .name(employeeRequest.getName())
                        .surname(employeeRequest.getSurname())
                        .department(employeeRequest.getDepartment())
                        .build();

        final Employee employeeSaved = employeeRepository.save(employee);
        return EmployeeResponse
                .builder()
                .name(employeeSaved.getName())
                .surname(employeeSaved.getSurname())
                .department(employeeSaved.getDepartment())
                .build();
    }
     */
    @Override
    public void createEmployee(EmployeeRequest employeeRequest) {
        Employee employeeMap = modelMapper.map(employeeRequest,Employee.class);

          employeeRepository.save(employeeMap);

    }

    @Override
    public EmployeeResponse updateEmployee(Long id, EmployeeRequest employeeRequest) {
        Employee employee =
                Employee
                        .builder()
                        .id(id)
                        .name(employeeRequest.getName())
                        .surname(employeeRequest.getSurname())
                        .department(employeeRequest.getDepartment())
                        .build();
        final Employee employeeSaved = employeeRepository.save(employee);
        return EmployeeResponse
                .builder()
                .name(employeeSaved.getName())
                .surname(employeeSaved.getSurname())
                .department(employeeSaved.getDepartment())
                .build();
    }

    @Override
    public void deleteEmployee(Long id) {
        employeeRepository.deleteById(id);
    }


    @Override
    public EmployeeResponse getEmployeeById(Long id) {

        final Employee employee = employeeRepository.findById(id)
                .orElseThrow();
        return EmployeeResponse
                .builder()
                .name(employee.getName())
                .surname(employee.getSurname())
                .department(employee.getDepartment())
                .build();

        /*
        EmployeeResponse
                .builder()
                .name(employee.getName())
                .surname(employee.getSurname())
                .department(employee.getDepartment())
                .build();
         */
    }


}
